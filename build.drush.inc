<?php
/**
 * Drush Build commands.
 */

/**
 * Implements hook_drush_command().
 */
function build_drush_command() {
  $commands = array();

  $commands['build'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => 'Build a Drupal site instance.',
    'examples' => array(
      'drush build' => 'Build the makefile in the current directory.',
    ),
  );

  return $commands;
}

/**
 * Find makefile.
 */
function drush_build() {
  $makefile = build_make_makefile_discovery();

  if ($makefile) {
    $make_data = make_parse_info_file($makefile);
    $build_data = $make_data['build'];

    // Abort if there's no path set in the makefile.
    if (!isset($build_data['path'])) {
      return drush_set_error(dt('Missing build path value in makefile %makefile', array('%makefile' => $makefile)));
    }

    $build_id = build_id($make_data);
    $build_dir = "build-$build_id";
    $build_path = $build_dir;

    if (isset($build_data['cache']) && isset($build_data['cache']['path'])) {
      $build_dir = $build_data['cache']['path'] . DIRECTORY_SEPARATOR . $build_dir;
      $build_path = $build_dir  . DIRECTORY_SEPARATOR . 'build';
      if (is_dir($build_dir)) {
        $make_result = TRUE;
      }
      else {
        drush_mkdir($build_data['cache']['path']);
        drush_mkdir($build_dir);
        $make_result = drush_build_build_make($makefile, $build_path);
        if (!$make_result) {
          drush_delete_dir($build_path);
        }
      }
    }
    else {
      $build_path = drush_find_tmp() . DIRECTORY_SEPARATOR . $build_path;
      $make_result = drush_build_build_make($makefile, $build_path);
    }

    if ($make_result) {
      drush_copy_dir($build_path, $build_data['path'], FILE_EXISTS_OVERWRITE);
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Try to find the project makefile. Returns FALSE if fails.
 */
function build_make_makefile_discovery() {
  $makefiles = glob('*.make');

  switch (count($makefiles)) {
    case 1:
      return end($makefiles);
    case 0:
      drush_log(dt('No makefile found in current directory.'));
      return FALSE;
    default:
      drush_log(dt('Two or more makefiles in current directory. Refusing to continue.'));
      drush_log(dt("Makefiles found: \n@makefiles", array('@makefiles' => implode("\n", $makefiles))));
      return FALSE;
  }
}

/**
 * Generate a build id based on the contents of a makefile.
 */
function build_id($data) {
  sort($data);
  return md5(serialize($data));
}

/**
 * Run drush make seamlessly for a given makefile and path.
 */
function drush_build_build_make($makefile, $path) {
  $tmp_path = drush_find_tmp() . DIRECTORY_SEPARATOR . $path . '-' . uniqid();
  $make_result = drush_invoke_process('@self', 'make', array($makefile, $tmp_path));
  drush_move_dir($tmp_path, $path, TRUE);
  return $make_result;
}
